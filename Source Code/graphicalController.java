//Internal Imports
import java.util.*;
import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.event.*;
import java.sql.*;
import javafx.geometry.*;
import java.net.*;
import java.io.*;
import javafx.stage.*;
import javafx.collections.*;
import java.awt.Desktop;
import javafx.scene.media.MediaPlayer.*;
import javafx.scene.text.Text;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.scene.control.*;
import javafx.scene.media.Media;
import javafx.scene.media.*;
import javafx.util.Duration;
import javafx.beans.Observable;
import javafx.scene.shape.*;
import javafx.animation.*;
import java.text.DecimalFormat;

//External Imports
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This is the controller for the GUI. Within the FXML code, the controlled class is declared using "fx:controller graphicalController".
 * As a result of this, FXML can only support one controller at a time. 
 * 
 * @author (Shaun Cockram) 
 * @version (v1.0.0)
 */
public class graphicalController implements Initializable 
{
    //GUI Decleration
    @FXML
    private Button centreButton;
    @FXML
    private Button backButton;
    @FXML
    private Button forwardButton;
    @FXML
    private ToggleButton muteToggle;
    @FXML
    private MenuItem loadFolder;
    @FXML
    private Text nameText;
    @FXML
    private Text albumText;
    @FXML
    private Text timeText;
    @FXML
    private Text artistText;
    @FXML
    private Slider timeSlider;
    @FXML
    private Slider volumeSlider;
    @FXML
    private Text volumeText;
    @FXML
    private ToggleButton loopToggle;

    //Controller Decleration
    private String absolutePath;
    private SongQueue q = new SongQueue();
    private MediaPlayer player;
    private boolean isPlaying = false;
    private boolean isMuted = false;
    private boolean isPaused = false;
    private boolean isLoaded = false;
    private boolean isLoading = false; 
    private boolean isLooping = false;
    private boolean hasPlayed = false;
    private boolean isDragging = false; 
    private Duration duration;
    private Duration totalTime;
    private Duration currentTime;
    private final DecimalFormat formatter = new DecimalFormat("00.00");
    private String totalTimeText = "";
    private String currentTimeText = ".";

    //Setting fade values in milliseconds. 
    private FadeTransition fadeOut = new FadeTransition(Duration.millis(1500));
    private FadeTransition fadeIn = new FadeTransition(Duration.millis(10));

    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     * 
     * The constructor must be here for initialize to work. Even if it has nothing within the constructor.
     * 
     * Anything in regards to CSS styling with FXML MUST be done within the initialize method.
     */

    public graphicalController() {

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */

    @FXML
    public void initialize(URL location, ResourceBundle resources)
    {
        //Setting text to be blank as well as setting pictures for parts of the GUI.
        centreButton.setStyle("-fx-background-image: url('/Resources/Play_Button.png')");
        centreButton.setText("");
        backButton.setStyle("-fx-background-image: url('/Resources/Back_Button.png')");
        backButton.setText("");

        forwardButton.setStyle("-fx-background-image: url('/Resources/Forward_Button.png')");
        forwardButton.setText("");

        muteToggle.setStyle("-fx-background-image: url('/Resources/ToggleSound_Medium_Button.png')");
        muteToggle.setText("");

        loopToggle.setStyle("-fx-background-image: url('/Resources/ToggleLoop_Button.png')");
        loopToggle.setText("");

        nameText.setText("");
        albumText.setText("");
        artistText.setText("");
        timeText.setText("");
        volumeText.setText("");

        //Disable all play sliders till songs are loaded.
        centreButton.setDisable(true);
        forwardButton.setDisable(true);
        backButton.setDisable(true);
        volumeSlider.setDisable(true);
        timeSlider.setDisable(true);
        muteToggle.setDisable(true);

        //Setting fading for volumePercentage (volumeText)
        fadeOut.setNode(volumeText);
        fadeOut.setFromValue(1);
        fadeOut.setToValue(0);
        fadeOut.setCycleCount(1);
        fadeOut.setAutoReverse(false);
        fadeIn.setNode(volumeText);
        fadeIn.setFromValue(1);
        fadeIn.setToValue(0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);

        /**
         * Adding listeners that cannot be done within FXML. There are listeners added to both the volume and time slider to
         * detect for a value change. Additionally, event handlers are added to the ToggleButtons within the program. 
         */

        volumeSlider.valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable,
                Number oldValue, Number newValue) {
                    double sliderValue = newValue.intValue();
                    handleVolumeSlider(sliderValue);
                    handleVolumeLoudness();
                }
            });

        timeSlider.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> obs, Boolean wasChanging, Boolean isNowChanging) {
                    isDragging = true;
                    if (! isNowChanging) {
                        //double sliderValue = newValue.intValue();
                        double sliderValue = timeSlider.getValue();
                        System.out.println(sliderValue);
                        updateTimeValuesSlider(sliderValue);
                        //updateScrubberValues(sliderValue);
                    }
                   
                }
            });

        muteToggle.setOnAction(a -> {
                handleVolumeMute();
                handleVolumeLoudness();
            });

        loopToggle.setOnAction(a -> {
                if(isLooping) {
                    isLooping = false;
                    System.out.println("Loop toggled off!");
                }
                else if (!isLooping) {
                    isLooping = true;
                    System.out.println("Loop toggled on!");
                }

            });

        /**
         * Program thread: Run
         * Description: Cycles every second (998 Milliseconds) to detect a change in time for song. Values on GUI are updated accordinly. 
         */    
        new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true)
                    {
                        if(isPlaying) {
                            currentTime = player.getCurrentTime();
                            totalTime = player.getMedia().getDuration();

                            updateTimeValues();
                            updateTimeSlider();

                            if(totalTimeText.equals(currentTimeText))
                            {
                                if(isLooping) {
                                    handlecentreButtonClick();
                                }
                                else {
                                    handleforwardButtonClick();
                                }
                            }
                            
                            try {Thread.sleep(498); }catch (Exception e){}
                        }
                        try {Thread.sleep(500);if(isDragging) {isDragging=false;}}catch (Exception e){}
                    }
                }
            }).start();

    }

    /**
     * Handles the user action on the play button.
     */
    public void handlecentreButtonClick() {
        //Checks to see if music has been toggled to loop. Otherwise, it will see if the song is currently playing or if the que is empty. 
        if (isLooping && (totalTimeText.equals(currentTimeText))) {
            //If song is looping, play the same song within the Media Player (player) from the start. 
            player.seek(totalTime.multiply(0));
        }
        else if(isPlaying)
        {
            //If the song is playing, pressing the centre button will cause the player to be paused. 
            player.pause();
            System.out.println("Paused!");
            centreButton.setStyle("-fx-background-image: url('/Resources/Play_Button.png')");
            isPaused = true;
            isPlaying = false;
        }
        else if(!(q.isEmpty())) { //If the queue is not empty...

            if(isPaused) //If the song is paused, it will start the song again. 
            {
                player.play();
                System.out.println("Playing!");
                centreButton.setStyle("-fx-background-image: url('/Resources/Pause_Button.png')");
            }
            else{ //Assuming that the song is neither paused or playing, this is the first time the song is being played, it will create the song into the media player. 
                String file = q.peek().fileName.toString();
                String path = absolutePath + "\\" + file; 
                Media song = new Media(new File(path).toURI().toString());
                player = new MediaPlayer(song);
                player.setVolume(volumeSlider.getValue() / 100);
                player.play();
                centreButton.setStyle("-fx-background-image: url('/Resources/Pause_Button.png')");
            }
            //Song is now playing, so set isPlaying true. 
            isPlaying = true;
            if (!hasPlayed) {
                //After first time playing, these sliders and toggles will be re-enabled. 
                volumeSlider.setDisable(false);
                muteToggle.setDisable(false);
                timeSlider.setDisable(false);
                hasPlayed = true;
            }
        }
        //Update metadata on screen with new data for new song. 
        setSongText();
    }

    /**
     * Handles the user action on the skip forward button.
     */
    public void handleforwardButtonClick() {
        //Stop song completly and remove from player. 
        System.out.println("Song skipped forward");
        player.stop();

        //Get the current song in queue and put it at the back. 
        Song currentSong = q.peek();
        q.push(currentSong);
        //Pop current song and move onto the next song. 
        q.pop();
        //Get the current song in queue and load it into the Media Player (player)
        String file = q.peek().fileName.toString();
        String path = absolutePath + "\\" + file; 
        Media song = new Media(new File(path).toURI().toString());
        player = new MediaPlayer(song);
        if(isPlaying) {
            player.play();
        }
        //Update metadata on screen with new data for new song. 
        setSongText();
        System.out.println("Current Song: " + q.peek().fileName);
    }

    /**
     * Handles the user action on the skip back button.
     */
    public void handlebackButtonClick() {
        //Stop song completly and remove from player. 
        System.out.println("Song skipped back");
        player.stop();

        //Getting list of files and seeing how many files there are in the directory. 
        File folder = new File(absolutePath);
        File[] listOfFiles = folder.listFiles();
        int listLength = listOfFiles.length; 

        //Loops and removes songs from front of the queue and moves it to the back. Will loop as many times as there are files in the directory.
        for (int k = 0; k < listLength-1; k++) {
            //Takes the current song and moves it to the back of que. Ending up with the original last song to be at the front of the queue. 
            Song currentSong = q.peek();
            q.push(currentSong);
            q.pop();
        }

        //Get the current song in queue and load it into the Media Player (player)
        String file = q.peek().fileName.toString();
        String path = absolutePath + "\\" + file; 
        Media song = new Media(new File(path).toURI().toString());
        player = new MediaPlayer(song);
        if(isPlaying) {
            player.play();
        }
        //Update metadata on screen with new data for new song. 
        setSongText();
        System.out.println("Current Song: " + q.peek().fileName);
    }

    /**
     * Handles the user action on the load folder menu button.
     * 
     * Description: Using windows explorer, it gets the directory of the users chosen folder and calls the method "loadFilesFromFolder" to load the files using this directory.  
     */
    public void handleLoadButtonClick() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(null);
        if(selectedDirectory == null)
        {  
            displayAlert("You have cancelled out of the directory chooser. No directory chosen. ");
        }
        else 
        {
            if(isLoaded) {
                if(!isLoading)
                {
                    isLoading = true; 
                }
            }

            absolutePath = selectedDirectory.getAbsolutePath();
            String path = absolutePath;
            loadFilesFromFolder(path);
        }

    }

    /**
     * Handles the user action on the volume slider when listener is fired.
     * 
     * Description: It will get the value of the slider, ranging from 0-100 in double data type, convert it to int and set the volume to the sliderValue/100 (i.e. 0.95)
     * It will then handle the fades and will fade in as soon as the slider is touched and fade out after 1.5 seconds of the slider being released. 
     */
    public void handleVolumeSlider(double sliderValue) {
        try {
            int currentPercenInt = (int) sliderValue;
            String currentPercenString = String.valueOf(currentPercenInt);
            fadeIn.playFromStart();
            volumeText.setText("Volume: " + currentPercenString +"%");
            player.setVolume(sliderValue/ 100);
            handleVolumeLoudness();
            fadeOut.playFromStart();
        } catch (Exception b) {}
    }

    /**
     * Handles the user action on the volume button when listener is fired.
     * 
     * Description: If the program is already muted, it will get the value of the volumeSlider and set the volume in accordance with this. 
     * If the music player (player) is not muted, it will set the volume to '0'. 
     */
    public void handleVolumeMute() {
        try{
            if(isMuted)
            {
                player.setVolume(volumeSlider.getValue() / 100);
                isMuted = false;
                volumeSlider.setDisable(false);
                handleVolumeLoudness();
            }
            else
            {
                player.setVolume(0);
                isMuted = true;
                volumeSlider.setDisable(true);
                handleVolumeLoudness();
            }
        } catch (Exception a) {}
    }

    /**
     *  When program is closed, this will be fired to ensure thread is closed. Having many threads running in the background will cause memeory leaks. Thus, important to close thread. 
     */
    public void handleCloseButtonClick() {
        try {
            System.exit(0);
        } catch (Exception c) {}
    }

    /**
     *  Description: When the volume slider listener is triggered, this will be called and update the image for the toggleMute button accordinly.
     *  Depending on how loud the volume is, the bigger the speaker will become. 
     */
    public void handleVolumeLoudness() {
        double volumeLevel = (volumeSlider.getValue() / 100);
        double volumeLevelMute = player.getVolume();

        if((volumeLevelMute == 0)) 
        {
            muteToggle.setStyle("-fx-background-image: url('/Resources/ToggleSound_Muted_Button.png')");
        }
        else if((volumeLevel <= 0.3)) 
        {
            muteToggle.setStyle("-fx-background-image: url('/Resources/ToggleSound_Small_Button.png')");
        }
        else if((volumeLevel > 0.3) && (volumeLevel <= 0.6)) 
        {
            muteToggle.setStyle("-fx-background-image: url('/Resources/ToggleSound_Medium_Button.png')");
        }
        else if((volumeLevel > 0.6)) 
        {
            muteToggle.setStyle("-fx-background-image: url('/Resources/ToggleSound_Button.png')");
        }
    }

    /**
     *  Description: This method essentially loads all files witihn a folder of the correct type into the queue ready to be accessed by the Media Player (player).
     */
    public void loadFilesFromFolder(String path) {
        if(isPlaying)
        {
            player.stop();
            centreButton.setStyle("-fx-background-image: url('/Resources/Play_Button.png')");
            isPlaying = false; 
            timeSlider.setValue(0);
        }
        //Declaring the folder path and getting list of files in folder. 
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        //If the queue is not emepty, it will pop all songs in queue. This happenes when another folder is loaded after one or more folders have already been loaded. 
        while(!(q.isEmpty()))
        {
            try {Thread.sleep(500);}catch (Exception e){}
            Song j = q.pop();
        }

        //Gets value for amount of files within a folder. 
        int listLength = listOfFiles.length; 
        //Only loops for amount of files there are in directory. 
        for (int k = 0; k < listLength; k++) {
            if (listOfFiles[k].isFile()) {
                //Gets full file name of the file. 
                String fileName = listOfFiles[k].getName();
                String fileNamePath = path + "\\" +fileName;

                //Checks to see if the file has the correct extension of MP3. If statement will be passed into if so. 
                String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
                String correctFileExtension = "mp3";
                if(correctFileExtension.equals(fileExtension)) {
                    try {
                        InputStream input = new FileInputStream(new File(fileNamePath));
                        ContentHandler handler = new DefaultHandler();
                        Metadata metadata = new Metadata();
                        Parser parser = new Mp3Parser();
                        ParseContext parseCtx = new ParseContext();
                        parser.parse(input, handler, metadata, parseCtx);
                        input.close();

                        //Getting meta data.
                        String songName = metadata.get("title");
                        String artistName = metadata.get("xmpDM:artist");
                        String albumName = metadata.get("xmpDM:album");
                        int id = k + 1;
                        //Pushes new song to queue with metadata. 
                        Song newSong = new Song(id, fileName, songName, artistName, albumName);
                        q.push(newSong);
                        //Update metadata on screen with new data for new song. 
                        setSongText();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SAXException e) {
                        e.printStackTrace();
                    } catch (TikaException e) {
                        e.printStackTrace();
                    }
                }
            }
            //If the files have never been loaded before, so first time, it will enable key GUI user buttons. 
            if (!isLoaded) {
                centreButton.setDisable(false);
                forwardButton.setDisable(false);
                backButton.setDisable(false);
                isLoaded = true;
            }
        } 
        if(isLoaded) {
            if(isLoading)
            {
                timeText.setText("Loading...");
            }
        }
    }

    /**
     *  Description: Gets metadata from current song in queue and updates values accordinly. 
     */
    public void setSongText() {
        //Getting metadata from current song in queue. 
        String file = q.peek().fileName;
        String songName = q.peek().songName;
        String albumName = q.peek().albumName;
        String artistName = q.peek().artistName;

        //If the song doesn't have any metadata for a certain field, it will tell the user on the GUI. 
        if (songName.equals("")) {
            songName = "Song name not specified in metadata.";
        }
        else if (albumName.equals(""))
        {
            albumName = " Album name not specified in metadata.";
        }
        else if (artistName.equals(""))
        {
            artistName = "Artist name not specified in metadata.";
        }
        //Updating GUI Text Fields. 
        nameText.setText(songName);
        albumText.setText(albumName);
        artistText.setText(artistName);
    }

    /**
     *  Description: Calculating time in String format of MM:SS from Duration. 
     */
    public void updateTimeValues () {
        //Converting duration (milliseconds) to seconds in String. 
        String currentTimeString = String.valueOf(formatter.format(Math.floor(currentTime.toSeconds())));
        String totalTimeString = String.valueOf(formatter.format(Math.floor(totalTime.toSeconds())));
        double currentTimeDouble = Double.parseDouble(currentTimeString);
        double totalTimeDouble = Double.parseDouble(totalTimeString);

        //Calculating for currentTime
        double currentTimeSecondsDouble = currentTimeDouble%60;
        double currentTimeMinutesDouble = currentTimeDouble/60;
        int currentTimeSecondsInt = (int) currentTimeSecondsDouble;
        int currentTimeMinutesInt = (int) currentTimeMinutesDouble;
        String currentTimeSecondsString = new DecimalFormat("00").format(currentTimeSecondsInt);
        String currentTimeMinutesString = String.valueOf(currentTimeMinutesInt);

        //Calculating for totalTime
        double totalTimeSecondsDouble = totalTimeDouble%60;
        double totalTimeMinutesDouble = totalTimeDouble/60;
        int totalTimeSecondsInt = (int) totalTimeSecondsDouble;
        int totalTimeMinutesInt = (int) totalTimeMinutesDouble;
        String totalTimeSecondsString = new DecimalFormat("00").format(totalTimeSecondsInt);
        String totalTimeMinutesString = String.valueOf(totalTimeMinutesInt);

        //Setting text
        String currentTimeStringText = (currentTimeMinutesString + ":" + currentTimeSecondsString);
        String totalTimeStringText = (totalTimeMinutesString + ":" + totalTimeSecondsString);
        String tempText = (currentTimeStringText + " | " + totalTimeStringText);

        //Updating text on GUI. 
        totalTimeText = totalTimeStringText;
        currentTimeText = currentTimeStringText;
        timeText.setText(tempText);
        System.out.println(tempText);
    }

    /**
     *  Description: Seeks to correct position of song.
     */
    public void updateTimeValuesSlider (double sliderValue) {
        //Pauses song to prevent irritating scrubbing noises from happening. 
        player.pause();
        sliderValue = sliderValue/100;
        //Seek to percentage of song. 
        player.seek(totalTime.multiply(sliderValue));
        if(isPlaying) {
            player.play();
        }
    }

    /**
     *  Description: Updates position of time slider each seconds. Called from thread and is update each second to move position of slider. 
     */
    public void updateTimeSlider () {
        if(isDragging)
        {
            System.out.println("test");
        }
        if(!isDragging)
        {
            String currentTimeString = String.valueOf(formatter.format(Math.floor(currentTime.toSeconds())));
            String totalTimeString = String.valueOf(formatter.format(Math.floor(totalTime.toSeconds())));
            double currentTimeDouble = Double.parseDouble(currentTimeString);
            double totalTimeDouble = Double.parseDouble(totalTimeString);
            double sliderPercentage = (currentTimeDouble/totalTimeDouble) * 100;
            timeSlider.setValue(sliderPercentage);
        }
    }

    /**
     *  Description: Opens webpage. 
     */
    public void openWebpage() throws Exception {
        try {
            if(Desktop.isDesktopSupported())
            {
                Desktop.getDesktop().browse(new URI("https://bitbucket.org/Jetrise/comsci-project/overview"));
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Description: Is called and passed in with a text for the alert box. Alert box is created however, to use CSS, you need to get the dialog pane and then attach the CSS. 
     */
    public void displayAlert (String alertText) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setContentText(alertText);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("css_alert.css").toExternalForm());
        dialogPane.getStyleClass().add("alert");
        alert.showAndWait();
    }

}
