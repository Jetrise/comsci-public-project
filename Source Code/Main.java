import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.shape.*;
import java.sql.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.*; 
import javafx.event.*;

/**
 * Main class will load CSS and FXML file into program. Then program will be lauched. 
 * 
 * @author (Shaun Cockram) 
 * @version (v1.0.0)
 */
public class Main extends Application {
    private Stage primaryStage;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            //Setting FXML file.
            this.primaryStage = primaryStage;
            AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("musicPlayer.fxml"));
            Scene scene = new Scene(page);
            
            //Setting CSS file.
            primaryStage.setScene(scene);
            primaryStage.setTitle("NEA: Music Player");
            scene.getStylesheets().add(getClass().getResource("css.css").toExternalForm());
            
            //Decorating stage and showing it to user. 
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}