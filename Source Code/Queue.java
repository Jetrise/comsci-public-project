
/**
 * interface queue.
 */
public interface Queue
{
    //note: interface methods are public anyway
    boolean isEmpty();
    void push(Song j);
    Song pop();
    Song peek();
}
