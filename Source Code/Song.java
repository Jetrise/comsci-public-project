import java.text.DecimalFormat;

/**
 * Song class is the structure for my Queue. This is the class used to create the actual song objects. 
 * 
 * @author (Shaun Cockram) 
 * @version (v1.0.0)
 */
public class Song
{
    // instance variables
    int id;
    String fileName;
    String songName;
    String artistName;
    String albumName;
    Song next;     
    
    /**
     * Constructors for objects of class Song
     */
    //default constructor with no data set
    public Song()
    {
    }
    //constructor of a Song, next not pointing to another Song
    public Song(int idIn, String fileNameIn, String songNameIn, String artistNameIn, String albumNameIn)
    {
        id = idIn;
        fileName = fileNameIn;
        songName = songNameIn;
        artistName = artistNameIn;
        albumName = albumNameIn;
        next = null;
    }
    //constructor of a Song with next not pointing to another Song
    /**
     * Methods
     */
    //override toString method which is default method when print object
    public String toString()
    {
        DecimalFormat df = new DecimalFormat("#000");
        return df.format(id)+ ", " + fileName + ", ";
    }

    public Song copy (){return new Song(this.id, this.fileName, this.songName, this.artistName, this.albumName);}
    
    
}
