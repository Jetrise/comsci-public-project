
/**
 * SongQueue.
 * Queue of Songs with FRONT of queue as first item and END of queue as last item. (Note: could be other way round)
 * Queue is a FIFO structure so remove items from front (pop) and add items to end (push)
 * because pop removes and returns first item the additional method peek is useful to get the first item but leave it in queue
 * 
 * @author (Shaun Cockram) 
 * @version (v1.0.0)
 */
public class SongQueue implements Queue
{
    Song first = null;
    //default constructor of list with no items
    public SongQueue(){}
    //constructor of list with one item
    public SongQueue(Song firstIn)
    {
        first = firstIn;
    }

    //methods to implement Queue interface: isEmpty, push, pop and peek
    public boolean isEmpty(){return this.first == null;}
    
    public void push(Song j) //add Song to end of queue
    {
        Song currentSong = null;
        if (this.isEmpty()) first = j;
        else
        {
            currentSong = this.first;
            while (currentSong.next != null) currentSong = currentSong.next;
            currentSong.next = j;
        }
    }
    public Song pop() //get and remove Song from front of queue
    {
        //code here
        Song j = null;
        if (!this.isEmpty())
        {
            j = this.first;                 //make j first Song in queue
            this.first = this.first.next;   //make next Song the new fisrt item
            j.next = null;                  //remove next pointer from j
        }
        return j;
    }
    
    public Song peek() //get but not remove Song from front of queue
    {
        //code here
        Song j = null;
        if (!this.isEmpty())
        {
            j = this.first.copy();                 //make j copy of first Song in queue
        }
        return j;
    }

    //count how many items are in this queue
    int length()
    {
        int count = 0;
        if (this.isEmpty()) return 0;
        count = 1;
        Song current = this.first;
        while (current.next !=null)
        {
            count++;
            current = current.next;
        }
        return count;
    }

    //override toString() object method: return string, line seperated, of all items in this list
    public String toString()
    {
        Song current = this.first;
        String result = "" + current;        //by default calls toString() method of current - alternative to current.toString()
        while (current.next !=null)
        {
            current = current.next;
            result = result + "\n" + current;
        }
        return result;
    }
}
